# Support

This project is here to provide information about the FIO projects.

# About FIO

FIO makes up a series of projects revolving around the collection, analysis and re-presentation of data for users of [Prosperous Universe](https://prosperousuniverse.com) (PrUn).

Originally, FIO was the name of the discord bot used by a PrUn corporation named FNAR Industries.  FIO was the FNAR Industries Oracle.  With the knowledge learned from that original adventure, FIO has become a shared resource for any player of PrUn.  FIO (at the time of this writing) is made up of several components:

- fnar/fiorest> is the REST server which operates as central point of collection of data, and offers most of it back to any users.
- fnar/fioextension> is a browser extension (currently for Chrome-based browsers) that can login to FIORest, and then sends it data.
- fnar/fioweb> is the web frontend utilized by users to reference data collected by the REST server.  Offers a lot of insight about the data; as well as data sharing functionality for users & groups.
- fnar/support> is this repository, which was created to manage supporting the FIO projects.

# Referencing a project's entries in issues

Since this is a support project, whos whole purpose is to be related to the multiple FIO projects, it will be useful in issues to be able to reference commits, merge requests, issues, etc in the individual projects.  Gitlab has official documentation on how to do this in your messages: https://docs.gitlab.com/ee/user/markdown.html#special-gitlab-references.

As a quick example, you would have something like this in your message:
```md
Referencing commit of fiorest@302612da, or as fiorest@302612da90500f49c1e3b8249d4a85b4dc49191a.  
Referencing issue of fiorest#7.  
Referencing merge request of fiorest!3.  
Referencing snippet fioextension$2084155.
```
and it would turn into this:  
> Referencing commit of fiorest@302612da, or as fiorest@302612da90500f49c1e3b8249d4a85b4dc49191a.  
> Referencing issue of fiorest#7.  
> Referencing merge request of fiorest!3.  
> Referencing snippet fioextension$2084155.


